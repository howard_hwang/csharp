﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emp;

namespace CtorDI
{
    public class EmployeeDAL : IEmployeeDAL
    {
        public List<Employee> SelectAllEmployees()
        {
            List<Employee> ListEmployees = new List<Employee>();
            //Get the Employees from the Database
            //for now we are hard coded the employees
            ListEmployees.Add(new Employee()
            {
                Id = 1,
                Name = "Pranaya",
                Department = "IT"
            });
            ListEmployees.Add(new Employee()
            {
                Id = 2,
                Name = "Kumar",
                Department = "HR"
            });
            ListEmployees.Add(new Employee()
            {
                Id = 3,
                Name = "Rout",
                Department = "Payroll"
            });
            return ListEmployees;
        }
    }

    public class EmployeeBLConstructor
    {
        public IEmployeeDAL employeeDAL;
        public EmployeeBLConstructor(IEmployeeDAL employeeDAL)
        {
            this.employeeDAL = employeeDAL;
        }
        public List<Employee> GetAllEmployees()
        {
            return employeeDAL.SelectAllEmployees();
        }
    }

    public class EmployeeBLProperty
    {
        private IEmployeeDAL employeeDAL;
        public IEmployeeDAL employeeDataObject
        {
            set
            {
                this.employeeDAL = value;
            }
            get
            {
                if (employeeDataObject == null)
                {
                    throw new Exception("Employee is not initialized");
                }
                else
                {
                    return employeeDAL;
                }
            }
        }
        public List<Employee> GetAllEmployees()
        {
            return employeeDAL.SelectAllEmployees();
        }
    }

    public class EmployeeBLMethod
    {
        public IEmployeeDAL employeeDAL;
        public List<Employee> GetAllEmployees(IEmployeeDAL _employeeDAL)
        {
            employeeDAL = _employeeDAL;
            return employeeDAL.SelectAllEmployees();
        }
    }
}
