﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp
{
    public interface IEmployeeDAL
    {
        List<Employee> SelectAllEmployees();
    }
}
