﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emp;
using CtorDI;

namespace CSharpStudy
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter number");
            string line = Console.ReadLine();
            if (int.TryParse(line, out int numberOfCards))
            {
                foreach (var card in CardPicker.PickSomeCards(numberOfCards))
                {
                    Console.WriteLine(card);
                }
            }
            else
            {
                Console.WriteLine("Please enter valid number");
            }
        }
    }
}
